#!/usr/bin/env coffee
# coffeelint: disable=max_line_length
ko = require "knockout"
child_process = require 'child_process'
fs = require 'fs'
notifier = require 'node-notifier'

threshold = 30
delay = 200

totalDelta = ko.observable(0)
clicksSaved = ko.observable(0)
clicksSaved.subscribe (newVal) ->
  console.log newVal
  unless newVal % 100
    notifier.notify
      title: "Autoclick"
      message: "#{clicksSaved()} clicks saved"
      time: 1000
      t: 1000
    console.log "saved #{clicksSaved()} clicks"


fs.readFile "#{__dirname}/savedClicks.txt", (err, content) ->
  clicksSaved(parseInt content if content and not err)

# write saved clicks
ko.computed ->
  return unless clicksSaved() > 5
  fs.writeFile "#{__dirname}/savedClicks.txt", clicksSaved(), ->
    console.log "stored clicks"
.extend({throttle:2000})

paused = ko.observable(false)
# Do autoclick
ko.computed ->
  return if paused()
  if totalDelta() > threshold
    totalDelta(0)
    clicksSaved(clicksSaved() + 1)
    child_process.exec "xte 'mouseclick 1'", (error, stdout, stderr) ->
      console.log error if error
      console.log stderr if stderr
      console.log stdout if stdout
      console.log "clicked"
.extend({throttle:delay})

# thresholdReset
ko.computed ->
  trigger = totalDelta()
  console.log "reset"
  totalDelta(0)
  trigger
.extend({throttle:delay + 1})

parse = (mouse, buffer) ->
  event =
    leftBtn: (buffer[0] & 1) > 0 # Bit 0
    rightBtn: (buffer[0] & 2) > 0 # Bit 1
    middleBtn: (buffer[0] & 4) > 0 # Bit 2
    xSign: (buffer[0] & 16) > 0 # Bit 4
    ySign: (buffer[0] & 32) > 0 # Bit 5
    xOverflow: (buffer[0] & 64) > 0 # Bit 6
    yOverflow: (buffer[0] & 128) > 0 # Bit 7
    xDelta: buffer.readInt8(1) # Byte 2 as signed int
    yDelta: buffer.readInt8(2) # Byte 3 as signed int

  if event.middleBtn and event.rightBtn
    paused !paused()

  if event.leftBtn and event.rightBtn
    event.middle = true

  if event.leftBtn or event.rightBtn or event.middleBtn
    event.type = "button"
  else
    event.type = "moved"
  event

Mouse = (mouseid) ->
  @wrap "onOpen"
  @wrap "onRead"
  @dev = (if typeof (mouseid) is "number" then "mouse" + mouseid else "mice")
  @buf = new Buffer(3)
  fs.open "/dev/input/" + @dev, "r", @onOpen
  return

fs = require("fs")
EventEmitter = require("events").EventEmitter
Mouse:: = Object.create(EventEmitter::,
  constructor:
    value: Mouse
)
Mouse::wrap = (name) ->
  self = this
  fn = this[name]
  this[name] = (err) ->
    return self.emit("error", err)  if err
    fn.apply self, Array::slice.call(arguments, 1)

  return

Mouse::onOpen = (fd) ->
  @fd = fd
  @startRead()
  return

Mouse::startRead = ->
  fs.read @fd, @buf, 0, 3, null, @onRead
  return

Mouse::onRead = ->
  event = parse(this, @buf)
  event.dev = @dev
  @emit event.type, event
  @startRead()  if @fd
  return

Mouse::close = ->
  fs.close @fd, (->
    # console.log this
    return
  )
  @fd = undefined
  return


###*
Sample Usage *
###

# read all mouse events from /dev/input/mice
mouse = new Mouse()
mouse.on "button", (event)->
  # unless event.ySign or event.xSign or event.xDelta or event.yDelta
    # clicksSaved(clicksSaved()-1)
  totalDelta(0)
  if event.middle
    child_process.exec "xte 'mouseclick 2'", (error, stdout, stderr) ->
      console.log error
      console.log stderr
      console.log stdout



mouse.on "moved", (e)->
  totalDelta totalDelta() + Math.abs e.xDelta
  totalDelta totalDelta() + Math.abs e.yDelta


  # console.log totalDelta()

# to read only a specific mouse by id (e.g. /dev/input/mouse0) use
# var mouse0 = newMouse(0);

# to close mouse
# mouse.close();
# mouse = undefined
